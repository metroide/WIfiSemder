package com.example.hp.wifisender;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

/**
 * Created by Germes on 15/03/2017.
 */

public class WiFiBroadcastReceiver extends BroadcastReceiver {
    private WifiP2pManager mManager;
    private WifiP2pManager.Channel channel;
    private MainActivity activity;

    public WiFiBroadcastReceiver(WifiP2pManager mManager, WifiP2pManager.Channel channel,
                                 MainActivity activity) {
        this.mManager = mManager;
        this.channel = channel;
        this.activity = activity;

        /**
         * @param mManager est associer au service android pour manager les wifi p2p
         * @param channel ecoute sur la chaine wifi p2p
         * @param activity est associer a l'activité qui ecoute le broadcast sur le wifi
         */
    }

    @Override

    // on analyse les actions de changement sur la connexion wifi

    public void onReceive(Context context, Intent intent) {
        String action=intent.getAction();

        /**
         * @param action determine l'action que l'on vien d'ecouté  a partir du broadcast
         */

        /**
         * on etudi les alertes system envoyer grace au broadcast
         * 1- on teste si l'etat du system a changer si tel est le cas
         *      a- on verifie si le wifi a ete activer
         *      b- si il a ete desactive et on notifi au system
         * 2- on teste si c'etat de connectivité p2p qui a changer si tel est le cas on notify
         * 3- on teste si la connexion 2p2 a été bien etablie
         * 4- enfin au teste si une action system est intervenu
         */

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)){
            /**
             * @param WifiP2pManager.EXTRA_WIFI_STATE contien l'etat du wifi
             */
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE,-1);

            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED){
                //le wifi est actif
                activity.setIsWifiP2pEnabled(true);
            } else {
                //le wifi n'est pas actif
                activity.setIsWifiP2pEnabled(false);
                activity.resetData();
            }

            Log.d("info", "etat du p2p a changer - " + state);

        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)){
            if (mManager!=null){
                mManager.requestPeers(channel,(WifiP2pManager.PeerListListener)activity
                        .getFragmentManager().findFragmentById(R.id.frag_list));
            }

            Log.d("info" , "l'etat du d'apairage a changer");

        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)){
            if (mManager == null){
                return;
            }

            // on fait une requete reseau

            NetworkInfo networkInfo=(NetworkInfo) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

            if (networkInfo.isConnected()){
                // on teste si notre wifi est connecter
                mManager.requestConnectionInfo(channel,(DeviceDetailFragment)activity
                .getFragmentManager().findFragmentById(R.id.frag_detail));
            } else {
                activity.resetData();
            }


        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)){
            DeviceListFragment fragment = (DeviceListFragment) activity.getFragmentManager()
                    .findFragmentById(R.id.frag_list);
            fragment.updateThisDevice((WifiP2pDevice) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE));
        }

    }
}
