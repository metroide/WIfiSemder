package com.example.hp.wifisender;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;


/**
 * Created by Germes on 15/03/2017.
 */

public class DeviceDetailFragment extends Fragment implements
        WifiP2pManager.ConnectionInfoListener {
    protected static final int CHOOSE_FILE_RESULT_CODE = 20;
    private View mContentView = null;
    private WifiP2pDevice device;
    private WifiP2pInfo info;
    ProgressDialog progressDialog = null;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        mContentView = inflater.inflate(R.layout.devise_detail,null);

        // connexion de l'utilisateur

        mContentView.findViewById(R.id.btn_connect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //recuperation de la configuration de l'hote
                WifiP2pConfig config = new WifiP2pConfig();
                config.deviceAddress=device.deviceAddress;
                config.wps.setup= WpsInfo.PBC;
                if (progressDialog != null && progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

                progressDialog=ProgressDialog.show(getActivity(),
                        "detail devise",
                        "presser sur cancelpour quitter",
                        true,
                        true,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {

                            }
                        });

                ((DeviceListFragment.DeviceActionListener)getActivity()).connect(config);
            }
        });

        // deconnexion de l'utilisateur


        mContentView.findViewById(R.id.btn_disconnect).setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DeviceListFragment.DeviceActionListener)getActivity()).disconnect();
            }
        });


        // demarage d'un nouveau client

        mContentView.findViewById(R.id.btn_start_client).setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // pour un debut on ne gere que l'envoi des images
                Intent intent=new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("images/*");
                startActivityForResult(intent,CHOOSE_FILE_RESULT_CODE);
            }
        });


        return mContentView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //notre result activy ne gere aue notre activité de choix de contenue
        if (resultCode == CHOOSE_FILE_RESULT_CODE){
            //recuperation du lien vers le fichier
            Uri uri = data.getData();
            // recuperation du statut du fichier
            TextView statusText = (TextView) mContentView.findViewById(R.id.status_text);
            statusText.setText("Sending: " + uri);
            Log.d(MainActivity.TAG, "Intent----------- " + uri);
            Intent serviceIntent = new Intent(getActivity(), FileTransferService.class);
            // definition de l'action a executé dans l'activité
            serviceIntent.setAction(FileTransferService.ACTION_SEND_FILE);
            //envoi de l'adresse uri du fichier a transferer
            serviceIntent.putExtra(FileTransferService.EXTRAS_FILE_PATH, uri.toString());
            //envoi de l'adresse de destination
            serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_ADDRESS,
                    info.groupOwnerAddress.getHostAddress());
            //envoi du port d'envoi du fichier
            serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_PORT, 8988);
            //demarage de l'activité d'envoi du fichier
            getActivity().startService(serviceIntent);
        }
    }

    /**
     *
     * @param wifiP2pInfo info sur la connectivité des hotes
     */

    public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {
        if (progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        this.info = wifiP2pInfo;
        this.getView().setVisibility(View.VISIBLE);

        // si la connexion a ete atablie les ip propietaire sont maintenant conue

        TextView view=(TextView)mContentView.findViewById(R.id.group_owner);
        view.setText(getResources().getString(R.string.group_owner_text)
            + ((info.isGroupOwner == true) ? getResources().getString(R.string.yes)
            : getResources().getString(R.string.no)));

        // adresse de la structure du wifi

        view=(TextView)mContentView.findViewById(R.id.device_info);
        view.setText("Group Owner IP - " + info.groupOwnerAddress.getHostAddress());


        //apres la recuperation de l'adresse de l'hote on peut proceder a l'envoie des fichier
        //envoie par les socket
        //pour chaque fichier on y associe un thread

        if (info.groupFormed && info.isGroupOwner){
            //appel de la tache asymcorne qui se charge de l'envoie du fichier
        } else if (info.groupFormed){
            // si l'utilisateur n'appartient pas au groupe on propose d'ajouter celui ci au groupe
            mContentView.findViewById(R.id.btn_start_client).setVisibility(View.VISIBLE);
            ((TextView) mContentView.findViewById(R.id.status_text)).setText(getResources()
                    .getString(R.string.client_text));

        }
    }



    // copy d'un fichier a partie d'un repertoire interne
    public static boolean copyFile(InputStream inputStream, OutputStream out) {
        byte buf[] = new byte[1024];
        int len;
        long startTime=System.currentTimeMillis();

        try {
            while ((len = inputStream.read(buf)) != -1) {
                out.write(buf, 0, len);
            }
            out.close();
            inputStream.close();
            long endTime=System.currentTimeMillis()-startTime;
            Log.v("","Time taken to transfer all bytes is : "+endTime);

        } catch (IOException e) {
            Log.d(MainActivity.TAG, e.toString());
            return false;
        }
        return true;
    }


    /**
     *
     * @param device l'element a afficher
     */
    public void showDetails(WifiP2pDevice device) {
        this.device = device;
        this.getView().setVisibility(View.VISIBLE);
        TextView view = (TextView) mContentView.findViewById(R.id.device_address);
        view.setText(device.deviceAddress);
        view = (TextView) mContentView.findViewById(R.id.device_info);
        view.setText(device.toString());

    }

    /**
     * suppression des elements de la vue
     */

    public void resetViews() {
        mContentView.findViewById(R.id.btn_connect).setVisibility(View.VISIBLE);
        TextView view = (TextView) mContentView.findViewById(R.id.device_address);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.device_info);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.group_owner);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.status_text);
        view.setText(R.string.empty);
        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
        this.getView().setVisibility(View.GONE);
    }


    /**
     * definition de la tache asynchone d'envoi des fichiers
     */

    public static class FileServerAsyncTask extends AsyncTask<Void, Void, String>{

        private Context context;
        private TextView statusText;

        public FileServerAsyncTask(Context context, TextView statusText) {
            this.context = context;
            this.statusText = statusText;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                ServerSocket serverSocket = new ServerSocket(8988);
                Log.d(MainActivity.TAG,"le serveur socket est ouvers");
                Socket client=serverSocket.accept();
                Log.d(MainActivity.TAG,"connexion reussi");

                final File f = new File(Environment.getExternalStorageDirectory() + "/"
                        + context.getPackageName() + "/wifip2pshared-" + System.currentTimeMillis()
                        + ".jpg");

                File dirs = new File(f.getParent());
                if (!dirs.exists())
                    dirs.mkdirs();
                f.createNewFile();

                Log.d(MainActivity.TAG,"copy du fichier"+f.toString());

                // connexion au repertoire client pour la copy du fichier
                //gace au serveur socket

                InputStream inputstream = client.getInputStream();
                copyFile(inputstream, new FileOutputStream(f));

                // fermeture du serveur apres la copy du serveur

                serverSocket.close();

                return f.getAbsolutePath();

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }
}
