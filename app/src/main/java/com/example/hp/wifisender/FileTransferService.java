package com.example.hp.wifisender;

/**
 * Created by hp on 16/03/2017.
 */

public class FileTransferService {
    /**
     * descriotion des paramettres
     */
    private static final int SOCKET_TIMEOUT = 5000;
    public static final String ACTION_SEND_FILE = "com.example.android.wifidirect.SEND_FILE";
    public static final String EXTRAS_FILE_PATH = "file_url";
    public static final String EXTRAS_GROUP_OWNER_ADDRESS = "go_host";
    public static final String EXTRAS_GROUP_OWNER_PORT = "go_port";

}
