package com.example.hp.wifisender;

import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Germes on 15/03/2017.
 */

/**
 * cette classe affiche la liste des points disponible et envoi une requete a l'activité
 * parent pour les differentes interaction avec les utilisateurs selon les evenements
 */


public class DeviceListFragment extends ListFragment implements WifiP2pManager.PeerListListener {

    /**
     *
     * @param savedInstanceState
     * @param peers : l'inste des points connecter
     * @param WifiP2PDevise : une instance de devise
     */

    private List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
    public ProgressDialog progressDialog=null;
    View mContentView=null;
    private WifiP2pDevice device;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setListAdapter(new WiFiPeerListAdapter(getActivity(),R.layout.row_device,peers));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContentView = inflater.inflate(R.layout.device_list,null);
        return mContentView;
    }

    /**
     *
     * @param wifiP2pDeviceList liste des dvice disponible
     */

    @Override
    public void onPeersAvailable(WifiP2pDeviceList wifiP2pDeviceList) {
        if (progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        peers.clear();
        peers.addAll(wifiP2pDeviceList.getDeviceList());
        ((WiFiPeerListAdapter)getListAdapter()).notifyDataSetChanged();
        if (peers.size() != 0){
            Log.d(MainActivity.TAG, "No devices found");
            return;
        }
    }

    /**
     * affacer la liste des devices sur l'application
     */

    public void clearPeers() {
        peers.clear();
        ((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();
    }

    public void onInitiateDiscovery() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = ProgressDialog.show(getActivity(), "Press back to cancel", "finding peers", true,
                true, new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
    }

    /**
     *
     * @param device information sur la devise au cas ou celle ci aurait changer
     */

    public void updateThisDevice(WifiP2pDevice device) {
        this.device = device;
        TextView view = (TextView) mContentView.findViewById(R.id.my_name);
        view.setText(device.deviceName);
        view = (TextView) mContentView.findViewById(R.id.my_status);
        view.setText(getDeviceStatus(device.status));
    }

    /**
     *
     * @return cette device
     */

    public WifiP2pDevice getDevice() {
        return device;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        WifiP2pDevice device = (WifiP2pDevice) getListAdapter().getItem(position);
        ((DeviceActionListener)getActivity()).showDetails(device);
    }

    /**
     * l'adapter sur la liste des devise (un adapter de type liste simple a implementé)
     */

    private class WiFiPeerListAdapter extends ArrayAdapter<WifiP2pDevice> {
        private List<WifiP2pDevice> items;

        /**
         *
         * @param context conserve le contex de l'activité
         * @param resource
         * @param items contien la liste des devise a adapter
         */

        public WiFiPeerListAdapter(Context context, int resource, List<WifiP2pDevice> items) {
            super(context, resource, items);
            this.items = items;
        }

        /**
         *
         * @param position la position de l'objet a adapter
         * @param convertView la vue contenant les informations sur la devise
         * @param parent le parent de la vue
         * @return la vue corespndant a une devise
         */
        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v==null){
                LayoutInflater vi=(LayoutInflater)getActivity().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_device,null);
            }

            WifiP2pDevice device=items.get(position);

            if (device != null){
                TextView nom = (TextView)v.findViewById(R.id.device_name);
                TextView status = (TextView)v.findViewById(R.id.device_details);

                if (nom != null){
                    nom.setText(device.deviceName);
                }
                if (status != null){
                    status.setText(getDeviceStatus(device.status));
                }
            }

            return v;
        }
    }




    /**
     *
     * @param deviceStatus le status
     * @return retourne le statut de la pair sous forme de String
     */

    private static String getDeviceStatus(int deviceStatus) {
        Log.d(MainActivity.TAG, "Peer status :" + deviceStatus);
        switch (deviceStatus) {
            case WifiP2pDevice.AVAILABLE:
                return "Available";
            case WifiP2pDevice.INVITED:
                return "Invited";
            case WifiP2pDevice.CONNECTED:
                return "Connected";
            case WifiP2pDevice.FAILED:
                return "Failed";
            case WifiP2pDevice.UNAVAILABLE:
                return "Unavailable";
            default:
                return "Unknown";
        }
    }


    public interface DeviceActionListener {

        void showDetails(WifiP2pDevice device);

        void cancelDisconnect();

        void connect(WifiP2pConfig config);

        void disconnect();
    }

}


